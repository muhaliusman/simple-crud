<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'admin', 'email' => 'admin@admin.com', 'password' => Hash::make('password'), 'created_at' => now()]
        ];
        DB::table('users')->insert($users);
    }
}
