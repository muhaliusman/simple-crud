<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $employees = [];
        $char = ['l', 'p'];
        for ($i=0; $i < 30; $i++) {
            $rand = mt_rand(0, 1);
            $employees[] = [
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'no_telephone'=> '08' . mt_rand(1000000000, 9999999999),
                'gender'=> $char[$rand],
            ];
        }


        DB::table('employees')->insert($employees);

    }
}
