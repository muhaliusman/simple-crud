<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                $validation = [
                    'name' => 'required',
                    'email' => 'required|unique:employees,email',
                    'no_telephone' => 'required|unique:employees,no_telephone',
                    'gender' => 'required',
                ];

                break;
            }
            case 'PUT':
            {
                $validation = [
                    'name' => 'required',
                    'email' => 'required|unique:employees,email,' . $this->employee,
                    'no_telephone' => 'required|unique:employees,no_telephone,' . $this->employee,
                    'gender' => 'required',
                ];

                break;
            }
            default: $validation = [];
        }

        return $validation;

    }
}
