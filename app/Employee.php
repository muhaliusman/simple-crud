<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['name', 'email', 'no_telephone', 'gender'];

    const LAKILAKI = 'l';
    const PEREMPUAN = 'p';

    public function getGenderAttribute() : string
    {
        if ($this->attributes['gender'] === self::LAKILAKI) {
            return 'Laki-Laki';
        } else {
            return 'Perempuan';
        }
    }
}
