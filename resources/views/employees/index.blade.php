@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Daftar Karyawan</strong> (Login untuk menambahkan data karyawan baru)</div>

                <div class="panel-body">
                    @if(auth()->user())
                    <a href="{{ route('employees.create') }}" class="btn btn-primary">Tambah Baru </a>
                    <hr>
                    @endif
                    <table class="table">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Nomer Telephone</th>
                            <th>Gender</th>
                            @if(auth()->user())
                            <th>Aksi</th>
                            @endif
                        </tr>
                        @foreach ($employees as $key => $employee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->email }}</td>
                            <td>{{ $employee->no_telephone }}</td>
                            <td>{{ $employee->gender }}</td>
                            @if(auth()->user())
                            <td>
                                <form method="POST" action="{{ route('employees.destroy', ['employee' => $employee->id]) }}">
                                    <a href="{{ route('employees.edit', ['employee' => $employee->id]) }}" class="btn btn-warning">Edit</a>
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </table>
                    {{ $employees->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
