@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="text-center">Simple Crud</h2>
    <hr>
    <div class="text-center">
        <a href="{{ route('employees.index') }}" class="btn btn-success">Lihat Data Karyawan</a>
    </div>
</div>
@endsection
